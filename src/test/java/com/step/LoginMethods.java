package com.step;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class LoginMethods {
	static WebDriver driver;
	
	@Given("Launch the browser and Login")
	public void launch_the_browser_and_login() {
		driver = new ChromeDriver();
	    driver.get("https://demowebshop.tricentis.com/");
	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	    driver.findElement(By.linkText("Log in")).click();
	    driver.findElement(By.id("Email")).sendKeys("pavipragya@gmail.com");
	    driver.findElement(By.name("Password")).sendKeys("pragya@123");
	    driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}

	@Then("Select the Books categories and add-to-cart")
	public void select_the_books_categories_and_add_to_cart() throws InterruptedException {
		
	    driver.findElement(By.xpath("//div[@class='header-menu']//ul[@class='top-menu']//a[@href='/books']")).click();
	    
	    WebElement hightolow = driver.findElement(By.id("products-orderby"));
	    Select s=new Select(hightolow);
	    s.selectByVisibleText("Price: High to Low");
	    driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
	    Thread.sleep(3000);
	    driver.findElement(By.xpath("(//input[@value='Add to cart'])[2]")).click();
	    
	    
	    
	}

	@Then("Select the electronic categories to add the Cellphones")
	public void select_the_electronic_categories_to_add_the_cellphones() throws InterruptedException {
	
	driver.findElement(By.xpath("(//a[@href='/electronics'])[1]")).click();
	driver.findElement(By.xpath("//h2[@class='title']//a[@href='/cell-phones']")).click();
	driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
	Thread.sleep(3000);
	driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
	
	//WebElement shoppingcart = driver.findElement(By.xpath("//span[text()='Shopping cart']"));
	//Actions a= new Actions(driver);
	//a.moveToElement(shoppingcart);
	
	String count = driver.findElement(By.xpath("//span[@class='cart-qty']")).getText();
	System.out.println(count);
	}

	@Then("Add the Gift cards")
	public void add_the_gift_cards() {
	    driver.findElement(By.xpath("(//a[@href='/gift-cards'])[1]")).click();
	    WebElement pageper = driver.findElement(By.id("products-pagesize"));
	    Select sel=new Select(pageper);
	    sel.selectByVisibleText("4");
	    driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
	    String display = driver.findElement(By.xpath("//div[@class='product-name']")).getText();
	    System.out.println(display);
	}

	@Then("Logout")
	public void logout() {
		driver.findElement(By.linkText("Log out")).click();
	}


}
